package lambdacallbackexample_maplistener;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 *
 * @author Daniel
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 */
public class HashMapWrapper<K, V> implements Map<K, V> {
    private final Map<K, V> delegatee = new HashMap<>();

    // Iteration Time Complexity doesn't matter because we are streaming/spliterating, not iterating
    private final Set<Consumer<Map<K, V>>> listeners = new HashSet<>();

    /**
     * Attach a listener that triggers a callback method when new Objects are added/removed from the Map
     * @param listener Listener to attach
     */
    public void addListener(Consumer<Map<K, V>> listener) {
        listeners.add(listener);
    }

    /**
     * Remove an attached listener
     * @param listener Listener to remove
     * @return {@code true} if specified listener exists
     */
    public boolean removeListener(Consumer<Map<K, V>> listener) {
        return listeners.remove(listener);
    }

    @Override
    public V put(K key, V value) {
        try {
            return this.delegatee.put(key, value);
        } finally {
            this.onChange();
        }
    }

    @Override
    public V remove(Object key) {
        try {
            return this.delegatee.remove(key);
        } finally {
            this.onChange();
        }
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        try {
            this.delegatee.putAll(m);
        } finally {
            this.onChange();
        }
    }

    @Override
    public void clear() {
        try {
            this.delegatee.clear();
        } finally {
            this.onChange();
        }
    }

    /**
     * LAMBDA + Stream: trigger callback method for each listener
     * <p>All listeners are triggered all at the same time in parallel
     */
    private void onChange() {
        this.listeners.stream()
                .forEach((listener) -> {
                    listener.accept(this.delegatee);
                });
        // this is equivalent to:
        /**
        ExecutorService executor = Executors.newFixedThreadPool(this.listeners.size());
        for (Consumer<Map<K, V>> listener : this.listeners) {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    listener.accept(HashMapWrapper.this.delegatee);
                }
            });
        }
        **/
    }

    @Override
    public int size() {
        return this.delegatee.size();
    }

    @Override
    public boolean isEmpty() {
        return this.delegatee.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.delegatee.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.delegatee.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return this.delegatee.get(key);
    }

    @Override
    public Set<K> keySet() {
        return this.delegatee.keySet();
    }

    @Override
    public Collection<V> values() {
        return this.delegatee.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return this.delegatee.entrySet();
    }
}
