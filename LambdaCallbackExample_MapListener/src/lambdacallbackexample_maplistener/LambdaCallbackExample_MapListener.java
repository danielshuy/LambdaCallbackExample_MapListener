package lambdacallbackexample_maplistener;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 *
 * @author Daniel
 */
public class LambdaCallbackExample_MapListener {
    private final HashMapWrapper<String, String> map;

    public LambdaCallbackExample_MapListener() {
        this.map = new HashMapWrapper<>();
        
        // LAMBDA: passing a function/method as an input parameter
        this.map.addListener(this::callback);
        // this is equivalent to:
        /**
        this.map.addListener(new Consumer<Map<String, String>>() {
            @Override
            public void accept(Map<String, String> t) {
                LambdaCallbackExample.this.callback(t);
            }
        });
        **/
    }
    
    public void add(String key, String value) {
        this.map.put(key, value);
    }

    private void callback(Map<String, String> map) {
        // LAMBDA + Stream: print each key-value pair
        map.entrySet().stream()
                .forEach((entry) -> {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    System.out.println(key + '=' + value);
                });
        // this is equivalent to:
        /**
        ExecutorService executor = Executors.newFixedThreadPool(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            executor.submit(new Runnable() {
                @Override
                public void run() {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    System.out.println(key + '=' + value);
                }
            });
        }
        **/
    }
    
    public static void main(String[] args) {
        LambdaCallbackExample_MapListener caller = new LambdaCallbackExample_MapListener();
        System.out.println("add()");
        caller.add("key1", "value1");
        System.out.println("add()");
        caller.add("key2", "value2");
    }
}
